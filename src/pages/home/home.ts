import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController } from 'ionic-angular';
import {FileTransfer, FileUploadOptions, FileTransferObject} from "@ionic-native/file-transfer";
import {Camera, CameraOptions} from "@ionic-native/camera";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  imageURI: any;
  imageFileName: any;
  private transfer: any;

  constructor(public navCtrl: NavController,
              private fileTransfer: FileTransfer,
              private camera: Camera,
              public loadingController: LoadingController,
              public toastCtrl: ToastController) {

  }

  //this gets the image from the Photo Library
  getImage() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }

    this.camera.getPicture(options).then((imageData) => {
      this.imageURI = imageData;
    }, (err) => {
      console.log(err);
      this.presentToast(err);
    });
  }



 uploadFile(){
  let loader = this.loadingController.create({
    content: "Uploading..."
  });
  loader.present();
  const fileTransfer: FileTransferObject = this.transfer.create();

  let options: FileUploadOptions = {
    fileKey: 'ionicfile',
    fileName: 'ionicfile',
    chunkedMode: false,
    mimeType: "image/jpeg",
    headers: {}
  }

  fileTransfer.upload(this.imageURI, 'http://92.168.1.119:8080/api/uploadImage', options)
  .then((data) => {
    console.log(data + "the upload is succesfull");
  }, (err) => {
    console.log(err);
    loader.dismiss();
    this.presentToast(err);
  })
}

  presentToast(msg) {
  let toast = this.toastCtrl.create({
    message: msg,
    duration: 3000,
    position: 'bottom'
  });

  toast.onDidDismiss(() => {
    console.log('Dismissed toast')
  });
  toast.present();
}



